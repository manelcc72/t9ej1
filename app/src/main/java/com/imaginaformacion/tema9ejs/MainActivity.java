package com.imaginaformacion.tema9ejs;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().setTitle("Mis Películas");


        MySQLiteHelper db = new MySQLiteHelper(this);


        db.deleteAllPeliculas();

        db.addPelicula(new Pelicula("El Caballero Oscuro", "Ciencia ficción", "http://ia.media-imdb.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1__SX1394_SY705_.jpg"));
        db.addPelicula(new Pelicula("El truco final", "Drama", "http://ia.media-imdb.com/images/M/MV5BMjA4NDI0MTIxNF5BMl5BanBnXkFtZTYwNTM0MzY2._V1__SX1394_SY705_.jpg"));
        db.addPelicula(new Pelicula("Interestellar", "Ciencia ficción", "http://ia.media-imdb.com/images/M/MV5BMjIxNTU4MzY4MF5BMl5BanBnXkFtZTgwMzM4ODI3MjE@._V1__SX1394_SY705_.jpg"));
        db.addPelicula(new Pelicula("Origen", "Ciencia ficción", "http://ia.media-imdb.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1__SX1394_SY705_.jpg"));


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new MyRecyclerAdapter(this, db.getAllPeliculas(), R.layout.card));


        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }
}
