package com.imaginaformacion.tema9ejs;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MySQLiteHelper extends SQLiteOpenHelper {

    // Versión de la base de datos
    private static final int DATABASE_VERSION = 1;
    // Nombre de la base de datos
    private static final String DATABASE_NAME = "PeliculaDB";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Sentencia SQL de creación de la tabla peliculas
        String CREATE_PELICULA_TABLE = "CREATE TABLE peliculas ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "genre TEXT, " +
                "image_url TEXT )";

        // Crear tabla "peliculas"
        db.execSQL(CREATE_PELICULA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Borrar tablas antiguas si existen
        db.execSQL("DROP TABLE IF EXISTS peliculas");

        // Crear tabla peliculas nueva
        this.onCreate(db);
    }
    //---------------------------------------------------------------------

    /**
     * Completar las operaciones CRUD de Pelicula
     */

    // Nombre de la tabla de Peliculas
    private static final String TABLE_PELICULAS = "peliculas";

    // Nombres de las columnas de la tabla de Peliculas
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_GENRE = "genre";
    private static final String KEY_IMAGE_URL = "image_url";

    private static final String[] COLUMNS = {KEY_ID, KEY_TITLE, KEY_GENRE, KEY_IMAGE_URL};

    public void addPelicula(Pelicula pelicula) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, pelicula.getTitle());
        values.put(KEY_GENRE, pelicula.genre);
        values.put(KEY_IMAGE_URL, pelicula.getImage_url());

        db.insert(TABLE_PELICULAS, null, values);

        db.close();
    }

    public Pelicula getPelicula(int id) {

        Pelicula pelicula = null;

        SQLiteDatabase db = this.getReadableDatabase();

        //TODO Obtener una Pelicula

        Cursor cursor = db.query(TABLE_PELICULAS, COLUMNS, "id = ?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            pelicula = new Pelicula();
            pelicula.setId(Integer.parseInt(cursor.getString(0)));
        }
        db.close();

        return pelicula;
    }

    public List<Pelicula> getAllPeliculas() {
        List<Pelicula> peliculas = new ArrayList<Pelicula>();

        SQLiteDatabase db = this.getWritableDatabase();

        String query = ("SELECT * FROM " + TABLE_PELICULAS);
        Cursor cursor = db.rawQuery(query, null);

        Pelicula pelicula = null;

        if (cursor.moveToFirst()) {
            do {
                pelicula = new Pelicula();
                pelicula.setId(Integer.parseInt(cursor.getString(0)));
                pelicula.setTitle(cursor.getString(1));
                pelicula.setGenre(cursor.getString(2));
                pelicula.setImage_url(cursor.getString(3));

                peliculas.add(pelicula);
            } while (cursor.moveToNext());

        }

        db.close();

        return peliculas;
    }

    public void updatePelicula(Pelicula pelicula) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("title", pelicula.getTitle());
        values.put("author", pelicula.getGenre());

        db.update(TABLE_PELICULAS,
                values,
                KEY_ID + " = ?",
                new String[]{String.valueOf(pelicula.getId())});

        db.close();


    }

    public void deletePelicula(Pelicula pelicula) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PELICULAS,
                KEY_ID + " = ?",
                new String[]{String.valueOf(pelicula.getId())});

        db.close();


    }

    public void deleteAllPeliculas() {


        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_PELICULAS);

        db.close();
    }
}