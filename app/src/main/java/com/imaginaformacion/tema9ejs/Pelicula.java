package com.imaginaformacion.tema9ejs;


public class Pelicula {

    int id;
    String title;
    String genre;
    String image_url;

    public Pelicula() {
    }

    public Pelicula(String title, String genre, String image_url) {
        this.title = title;
        this.genre = genre;
        this.image_url = image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return "Pelicula{" +
                "title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}
